% Travis Llado, TAL686
% ASE381P Optimal Control Theory
% Homework 3
% 2016 Oct 24
% Written for MATLAB R2016a

% As an initial sanity check, we plot our functions, its equality
% constraints, and find the minimum manually.
% We simplify the original problem by solving h(x1,x2) for x1 and inserting
% this into f(x1,x2).
% Converting h(x1,x2) to x1(x2), we get
% x1(x2) = (-1 +- sqrt(1-4*(x2 + 0.5)^2)/2
% or
% x1(x2) = -0.5 +- sqrt(0.25 - (x2 + 0.5)^2)
% Then, f(x1,x2) is real for x1=(-1,0), x2=(-1,0).

% min(f(x1,x2)) occurs at: x1=-0.24634, x2=-0.06912

clear variables;
clc;

x1Min = -1;
x1Max = 0;
x1Res = 0.001;
x2Min = -1;
x2Max = 0;
x2Res = 0.001;
eps = 0.0005;

x1 = x1Min:x1Res:x1Max;
x2 = (x2Min:x2Res:x2Max)';
f = zeros(length(x1),length(x2));
h = f;
h0 = 0;
h0x1 = 0;
h0x2 = 0;
h0Count = 1;
x1 = repmat(x1,length(x1),1);
x2 = repmat(x2,1,length(x2));

xMinC = -1;
xMaxC = 0;
xResC = 0.00001;

x1C = xMinC:xResC:xMaxC;
x2C = x1C;
halfLengthC = length(x1C);
x2C = [x2C x2C];
fC = zeros(length(x2C),1);

for ii = 1:length(x1)
    for jj = 1:length(x2')
        f(ii,jj) = 100*(x2(ii,jj) - x1(ii,jj)^2)^2 + (1 - x1(ii,jj))^2;
        h(ii,jj) = (x1(ii,jj) + 0.5)^2 + (x2(ii,jj) + 0.5)^2 - 0.25;
        if(abs(h(ii,jj))<eps)
            h0(h0Count) = h(ii,jj);
            h0x1(h0Count) = x1(ii,jj);
            h0x2(h0Count) = x2(ii,jj);
            h0Count = h0Count + 1;
        end
    end
end

for ii = 1:halfLengthC
    % Calculate x1_upper(x2)
    x1C(ii) = -0.5 + sqrt(0.25 - (x2C(ii) + 0.5)^2);
    % Calculate x1_lower(x2)
    x1C(ii + halfLengthC) = -0.5 - sqrt(0.25 - (x2C(ii) + 0.5)^2);
    % Calculate f_upper(x1,x2)
    fC(ii) = 100*(x2C(ii) - x1C(ii)^2)^2 + (1 - x1C(ii))^2;
    % Calculate f_upper(x1,x2)
    fC(ii + halfLengthC) = 100*(x2C(ii) - x1C(ii + halfLengthC)^2)^2 +...
        (1 - x1C(ii + halfLengthC))^2;
end

hold on;
fsurf = surfl(x1,x2,f);
set(fsurf,'edgecolor','none');
hsurf = surf(x1,x2,h);
set(hsurf,'edgecolor','none');
plot3(h0x1,h0x2,h0,'r.')
plot3(x1C,x2C,fC,'.');
[~,fMin] = min(fC);
plot3(x1C(fMin),x2C(fMin),fC(fMin),'+');
title('f(x1,x2) - green, h(x1,x2) - blue');
xlabel('x1');
ylabel('x2');
zlabel('z');

disp(['min(f(x1,x2)) occurs at: x1=' num2str(x1C(fMin)) ', x2='...
    num2str(x2C(fMin))]);
disp('h(x1,x2)=0');
