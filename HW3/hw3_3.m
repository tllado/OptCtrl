% Travis Llado, TAL686
% ASE381P Optimal Control Theory
% Homework 3
% 2016 Oct 24
% Written for MATLAB R2016a

% Minimizing Rosenbrock's Function contrained by the equality h(x1,x2) = 0
% using a combination of Lagrange Multipliers then Penalties.
% Produces:
% min(f(x1,x2)) occurs at: x1=-0.24637, x2=-0.069072
% h(x1,x2)=2.9945e-05 after 10 iterations

% 2016 Oct 25 Update
% Discovered that algorithm is incorrect, need to update lambda prior to
% alpha. This causes algorithm to converge much faster and more
% consistently with default parameters.
% Produces:
% min(f(x1,x2)) occurs at: x1=-0.24634, x2=-0.069083
% h(x1,x2)=3.0781e-05 after 10 iterations

function hw3_3()
    clc;
    global alphaPhi;
    alphaPhi = 0.1;
    betaPhi = 6;
    global alphaPsi;
    alphaPsi = 0.1;
    betaPsi = 6;
    global lambda;
    lambda = 10;
    ii = 1;
    x0 = [1 1];
    epsilon = 10^-4;
    
    xMin(1,:) = x0;
    hErr(1) = (x0(1) + 0.5)^2 + (x0(2) + 0.5)^2 - 0.25;
    
    while abs(hErr(ii)) > epsilon
        ii = ii + 1;
            alphaPhi = alphaPhi*betaPhi;
            lambda = lambda + alphaPsi*((xMin(ii - 1, 1) + 0.5)^2 +...
                (xMin(ii - 1, 2) + 0.5)^2 - 0.25);
            alphaPsi = alphaPsi*betaPsi;
        if ii == 2
            xMin(ii, :) = fminsearch(@psi,xMin(ii - 1, :));
        else
            xMin(ii, :) = fminsearch(@phi,xMin(ii - 1, :));
        end
        hErr(ii) = (xMin(ii, 1) + 0.5)^2 + (xMin(ii, 2) + 0.5)^2 - 0.25;
    end
    
    figure;
    subplot(1,2,1);
    surface([xMin(:,1)';xMin(:,1)'],[xMin(:,2)';xMin(:,2)'],[hErr;hErr],...
        [1:length(hErr);1:length(hErr)],'facecol','no',...
        'edgecol','interp','linew',2);
    title('Iterative minimization of f(x1,x2)');
    xlabel('x1');
    ylabel('x2');
    subplot(1,2,2);
    surface([1:length(hErr);1:length(hErr)],[hErr;hErr],[hErr;hErr],...
        [1:length(hErr);1:length(hErr)],'facecol','no',...
        'edgecol','interp','linew',2);
    title('Iterative Decrease of h(x1,x2)');
    xlabel('Iteration #');
    ylabel('h Error');
    
    disp(['min(f(x1,x2)) occurs at: x1=' num2str(xMin(length(xMin),1))...
        ', x2=' num2str(xMin(length(xMin),2))]);
    disp(['h(x1,x2)=' num2str(hErr(length(hErr))) ' after ' num2str(ii)...
        ' iterations']);
end

function output = phi(x)
    global alphaPhi;
    
    f = 100*(x(2) - x(1)^2)^2 + (1 - x(1))^2;
    h = (x(1) + 0.5)^2 + (x(2) + 0.5)^2 - 0.25;
    output = f + alphaPhi/2*h^2;
end

function output = psi(x)
    global alphaPsi;
    global lambda;
    
    f = 100*(x(2) - x(1)^2)^2 + (1 - x(1))^2;
    h = (x(1) + 0.5)^2 + (x(2) + 0.5)^2 - 0.25;
    output = f + lambda*h + alphaPsi/2*h^2;
end
