% Travis Llado, TAL686
% ASE381P Optimal Control Theory
% Homework 3
% 2016 Oct 24
% Written for MATLAB R2016a

% Problem 1
% Minimizing Rosenbrock's Function contrained by the equality h(x1,x2) = 0
% using a penalty method.

% Produces:
% min(f(x1,x2)) occurs at: x1=-0.24633, x2=-0.069107
% h(x1,x2)=1.8257e-05 after 9 iterations
% If we crank our parameters up to alpha = 1000, beta = 10, we converge in
% only four iterations. With alpha = 100000, beta = 10, we converge in two
% iterations. Of course, this tuning is based around a single test, and we
% should not expect these parameters to work for different tests.

function hw3_1()
    clc;
    global alpha;
    alpha = 0.1;
    beta = 6;
    ii = 1;
    x0 = [1 1];
    epsilon = 10^-4;
    
    xMin(1,:) = x0;
    hErr(1) = (x0(1) + 0.5)^2 + (x0(2) + 0.5)^2 - 0.25;
    
    while abs(hErr(ii)) > epsilon
        ii = ii + 1;
        alpha = alpha*beta;
        xMin(ii, :) = fminsearch(@phi,xMin(ii - 1, :));
        hErr(ii) = (xMin(ii, 1) + 0.5)^2 + (xMin(ii, 2) + 0.5)^2 - 0.25;
    end
    
    figure;
    subplot(1,2,1);
    surface([xMin(:,1)';xMin(:,1)'],[xMin(:,2)';xMin(:,2)'],[hErr;hErr],...
        [1:length(hErr);1:length(hErr)],'facecol','no',...
        'edgecol','interp','linew',2);
    title('Iterative minimization of f(x1,x2)');
    xlabel('x1');
    ylabel('x2');
    subplot(1,2,2);
    surface([1:length(hErr);1:length(hErr)],[hErr;hErr],[hErr;hErr],...
        [1:length(hErr);1:length(hErr)],'facecol','no',...
        'edgecol','interp','linew',2);
    title('Iterative Decrease of h(x1,x2)');
    xlabel('Iteration #');
    ylabel('h Error');
    
    disp(['min(f(x1,x2)) occurs at: x1=' num2str(xMin(length(xMin),1))...
        ', x2=' num2str(xMin(length(xMin),2))]);
    disp(['h(x1,x2)=' num2str(hErr(length(hErr))) ' after ' num2str(ii)...
        ' iterations']);
end

function output = phi(x)
    global alpha;
    
    f = 100*(x(2) - x(1)^2)^2 + (1 - x(1))^2;
    h = (x(1) + 0.5)^2 + (x(2) + 0.5)^2 - 0.25;
    output = f + alpha/2*h^2;
end
