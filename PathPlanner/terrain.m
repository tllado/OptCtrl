function terrain()
    % Generate Random Terrain
    res = 500;
    height = 100;
    F = 3;
    [X, Y] = ndgrid(1:res,1:res);
    i = min(X - 1,res - X + 1);
    j = min(Y - 1,res - Y + 1);
    H = exp(-.5*(i.^2 + j.^2)/F^2);
    Z = real(ifft2(H.*fft2(randn([res res]))))*20*height;
    
    % Calculate gradient map
    G = zeros(res - 1, res - 1, 2);
    for ii = 1:res - 1
        for jj = 1:res - 1
            G(ii, jj, 1) = (Z(ii + 1, jj) - Z(ii, jj))/2;
            G(ii, jj, 2) = (Z(ii, jj + 1) - Z(ii, jj))/2;
        end
    end

    % Calculate Direct Path
    start = round(res*0.1);
    finish = round(res*0.9);
    direct = zeros(finish - start,3);
    for ii = start:finish
        direct(ii - start + 1, 1) = ii;
        direct(ii - start + 1, 2) = ii;
        direct(ii - start + 1, 3) = Z(ii, ii);
    end

    % Calculate Path Costs
    costDir = num2str(costPath(direct));
    last = length(direct);
    start = direct(1, :);
    finish = direct(last, :);
    costStr = num2str(costPath([start; finish]));

    % Calculate optimal plane
    m = (finish(3) - start(3))/(finish(2) + finish(1) - start(2) - start(1));
    zOpt = (X + Y - start(1) - start(2))*m + start(3);
    
    % Calculate cost plane
    zCost = Z - zOpt;
    
    % Calculate gradient map for cost plane
    G = zeros(res - 1, res - 1, 2);
    for ii = 1:res - 1
        for jj = 1:res - 1
            G(ii, jj, 1) = (zCost(ii + 1, jj) - zCost(ii, jj))/2;
            G(ii, jj, 2) = (zCost(ii, jj + 1) - zCost(ii, jj))/2;
        end
    end
    
    % Calculate Optimized Path
    optimal = direct;
    new = [0 0 0];
    for jj = 1:15
        for ii = 2:length(optimal(:, 1)) - 1
            new(1) = optimal(ii, 1) - sign(G(optimal(ii, 1), optimal(ii, 2), 1));
            new(2) = optimal(ii, 2) - sign(G(optimal(ii, 1), optimal(ii, 2), 2));
            new(3) = Z(new(1), new(2));
    %         if(new ~= optimal(ii - 1, :) & new ~= optimal(ii + 1, :))
                optimal(ii, :) = new;
    %         end
        end
    
        for ii = 1:length(optimal(:, 1)) - 1
            if(sqrt((optimal(ii + 1, 1) - optimal(ii, 1))^2 + (optimal(ii + 1, 2) - optimal(ii, 1))^2) > 25)
                optimal = [ optimal(1:ii, :); 
                            [round((optimal(ii, 1) + optimal(ii + 1, 1))/2) round((optimal(ii, 2) + optimal(ii + 1, 2))/2) 0];
                            optimal(ii + 1:length(optimal(:, 1)), :)   ];
                optimal(ii + 1, 3) = Z(optimal(ii + 1, 1), optimal(ii + 1, 2));
            end
        end
    end
    
    costOpt = num2str(costPath(optimal));

    % Draw pictures
    s1 = surf(X, Y, zOpt, 'edgecolor', 'none');
    hold all;
%     s2 = surf(X, Y, zCost, 'edgecolor', 'none');
    alpha 0.3
    s3 = surf(X, Y, Z, 'edgecolor', 'none');
    light;
    p1 = plot3([start(1) finish(1)], [start(2) finish(2)], [start(3) finish(3)], 'k', 'LineWidth', 3);
    p2 = plot3(direct(:, 1), direct(:, 2), direct(:, 3), 'r', 'LineWidth', 3);
    p3 = plot3(optimal(:, 1), optimal(:, 2), optimal(:, 3), 'g', 'LineWidth', 3);
    legend([s3 s1 p1 p2 p3], 'Terrain', 'Optimal Plane', 'Straight Line', ['Direct Path: ' costDir], ['Optimized Path: ' costOpt]);
    axis equal
end

%Calculate cost of entire path
function cost = costPath(x)
    cost = 0;
    for ii = 1:length(x(:,1)) - 1
        cost = cost + costStep(x(ii, :), x(ii + 1, :));
    end
end

% Calculate cost of single step
function output = costStep(x0, xf)
    if(x0 == xf)
        output = 0;
    else
        output = exp((xf(3) - x0(3))*10 ...
        /sqrt((xf(1) - x0(1))^2 + (xf(2) - x0(2))^2)) ...
        *sqrt((xf(1) - x0(1))^2 + (xf(2) - x0(2))^2 + (xf(3) - x0(3))^2);
    end
end